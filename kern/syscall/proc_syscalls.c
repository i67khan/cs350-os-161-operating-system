#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include <clock.h>
#include <array.h>
#include "../arch/mips/include/trapframe.h"
#include <synch.h>
#include "../include/syscall.h"
#include "../include/types.h"
#include "../arch/mips/include/types.h"
#include <kern/fcntl.h>
#include <vm.h>
#include <vfs.h>
#include "opt-A2.h"

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);
  #if OPT_A2
    for (unsigned int i = 0; i < array_num(p->p_children); i++) {
      struct proc *temp_child = (struct proc *) array_get(p->p_children, i);
      array_remove(p->p_children, i);
      spinlock_acquire(&temp_child->p_lock);
      if (temp_child->p_exitstatus == __WEXITED) {
        spinlock_release(&temp_child->p_lock);
        proc_destroy(temp_child);  //ERROR FOUND HERE IDK WHY PLS HELP
      } else {
        temp_child->p_parent = NULL;
        spinlock_release(&temp_child->p_lock);
      }
    }
  #endif
  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  #if OPT_A2
    spinlock_acquire(&p->p_lock);
    if (p->p_parent == NULL) {
      spinlock_release(&p->p_lock);
      proc_destroy(p);
    } else {
      KASSERT(p->p_exitstatus == -1);
      p->p_exitstatus = __WEXITED;
      KASSERT(p->p_exitstatus == 0);
      p->p_exitcode = _MKWAIT_EXIT(exitcode);
      spinlock_release(&p->p_lock);
      cv_signal(p->exited,p->child_lk);
    }
  #else
    proc_destroy(p);
  #endif

  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  #if OPT_A2
    *retval = curproc->p_pid;
  #else
    *retval = 1;
  #endif
  return 0;
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
}

/* stub handler for waitpid() system call                */

#if OPT_A2
  int sys_fork(pid_t *retval,
        struct trapframe *tf) {
          struct proc *proc;
          proc = proc_create_runprogram("child");
          KASSERT(proc != NULL);
          proc->p_parent = curproc;
          array_add(proc->p_parent->p_children,(void *)proc,NULL);
          int err = as_copy(curproc_getas(), &(proc->p_addrspace));
          KASSERT(err == 0);
          struct trapframe *trapframe_for_child = kmalloc(sizeof(struct trapframe));
          KASSERT(trapframe_for_child != NULL);
          memcpy(trapframe_for_child, tf, sizeof(struct trapframe));
          // trapframe_for_child = tf;
          thread_fork("child_thread", proc, (void *)&enter_forked_process, (void *)trapframe_for_child, 0);
          *retval = proc->p_pid;              
          clocksleep(1);
          return (0);

        }
#endif

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

#if OPT_A2
  struct proc *p = curproc;
  struct proc *temp_child;  
  lock_acquire(p->child_lk);  
  for (unsigned int i = 0; i < array_num(p->p_children); i++) {
    temp_child = (struct proc *) array_get(p->p_children, i);
    if (temp_child->p_pid == pid) {
      array_remove(p->p_children, i);
      break;
    }    
  }
  if (temp_child && temp_child->p_pid != pid) {
    *retval = -1;
    return(ESRCH);
  }
  while (temp_child->p_exitstatus == -1) {    
    cv_wait(temp_child->exited, temp_child->child_lk);
  }
  exitstatus = temp_child->p_exitcode;
  lock_release(p->child_lk);
  proc_destroy(temp_child);
  (void) options;
#else
  if (options != 0) {
    return(EINVAL);
  }
  /* for now, just pretend the exitstatus is 0 */
  exitstatus = 0;
#endif
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}


 

//-------------------------------------------------------------------------------------
#if OPT_A2
int execv(char *program, char **args) {
  struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;
  int argc = 0;

  // Count the number of arguments and copy them into the kernel.
  for (int i = 0; args[i] != NULL; i++) {
    argc++;
  }
  // Copy the program path from user space into the kernel.
  size_t progname_size = strlen(program) + 1;
  char *program_kern_cpy = kmalloc(progname_size);
  int err = copyin((const_userptr_t)program, (void *) program_kern_cpy, progname_size);
  KASSERT(err == 0);
  kprintf("RUNNING PROGRAM: %s\n",program_kern_cpy);
  kprintf("PROGRAM ARGS = %d\n",argc);

  // Copy program args into kernel
  char ** args_kernel = kmalloc((argc + 1) * sizeof(char *));
  KASSERT(args_kernel != NULL);

  for(int i = 0; i <= argc; i++) {
    if (i == argc) {
      args_kernel[i] = NULL;
    } else {
      size_t arg_size = (strlen(args[i]) + 1) * sizeof(char);
      args_kernel[i] = kmalloc(arg_size);
      KASSERT(args_kernel[i] != NULL);
      int err = copyinstr((const_userptr_t) args[i], (void *) args_kernel[i], 128, &arg_size);
      KASSERT(err == 0);
    }
  }

	/* Open the file. */
	result = vfs_open(program_kern_cpy, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* We should be a new process. */
	KASSERT(curproc_getas() != NULL);

	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	struct addrspace *oldproc_as = curproc_setas(as);
	as_activate();
  
	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

  vaddr_t temp_stack_ptr = stackptr;
  vaddr_t *stack_args = kmalloc((argc + 1) * sizeof(vaddr_t));

  for(int i = argc; i >= 0; i--) {
    if (i == argc) {
      stack_args[i] = (vaddr_t) NULL;
      continue;
    }
    size_t arg_length = ROUNDUP(strlen(args_kernel[i]) + 1, 4);
    size_t arg_size = arg_length * sizeof(char);
    temp_stack_ptr -= arg_size;
    int err = copyoutstr((void *) args_kernel[i], (userptr_t) temp_stack_ptr, 128, &arg_length);
    KASSERT(err == 0);
    stack_args[i] = temp_stack_ptr;
  }

  for(int i = argc; i >= 0; i--) {
    size_t str_pointer_size = sizeof(vaddr_t);
    temp_stack_ptr -= str_pointer_size;
    int err = copyoutstr((void *) &stack_args[i], (userptr_t) temp_stack_ptr, 128, &str_pointer_size);
    KASSERT(err == 0);
  }

  as_destroy(oldproc_as);
  kfree(program_kern_cpy);

	/* Warp to user mode. */
	enter_new_process(argc, (userptr_t) temp_stack_ptr /*userspace addr of argv*/,
			  ROUNDUP(temp_stack_ptr, 8), entrypoint);
	
	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
}
#endif
